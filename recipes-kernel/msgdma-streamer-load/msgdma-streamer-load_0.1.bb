SUMMARY = "MSGDMA-Streamer Kernel Load Script"
SECTION = "base"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=12f884d2ae1ff87c09e5b7ccc2c4ca7e"

SRC_URI = "git://gitlab.com/feddischson/msgdma-streamer.git;protocol=https;rev=85e27c12629954f1adfd94b0c837da79cbc93cea"

do_compile () {
}

do_install () {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/git/linux/msgdma_streamer_load.sh ${D}${bindir}/
}

S = "${WORKDIR}/git/linux"
