SUMMARY = "MSGDMA-Streamer Kernel Module"
LICENSE = "GPLv2"

LIC_FILES_CHKSUM = "file://COPYING;md5=12f884d2ae1ff87c09e5b7ccc2c4ca7e"

inherit module

SRC_URI = "git://gitlab.com/feddischson/msgdma-streamer.git;protocol=https;rev=85e27c12629954f1adfd94b0c837da79cbc93cea"

SRC_URI[md5sum] = "343b02355da2e78637827a65c533f473"

S = "${WORKDIR}/git/linux"

RPROVIDES_${PN} += "kernel-module-msgdma-streamer"
